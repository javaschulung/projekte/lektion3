package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

/**
 * Die Hauptklasse zum Starten des Programms.
 * 
 * Implementiere hier deine eigenen Drachenfarm(en).
 * 
 * @author ...
 *
 */
public class HauptKlasse {
	
	/**
	 * Wandelt das übergebene Datum in eine entsprechende Textdarstellung um.
	 * 
	 * @param datum Das Datum welches umzuwandeln ist
	 * @return Eine Textdarstellung des übergebenen Datums
	 */
	public static String datumZuString(Datum datum) {
		return "" + String.format("%02d", datum.getTag()) + "." + String.format("%02d", datum.getMonat()) + "." + datum.getJahr();
	}

	/**
	 * Gibt den Züchter auf der Konsole aus.
	 * 
	 * @param zuechter Der Züchter der auszugeben ist
	 */
	public static void ausgabeZuechter(DrachenZuechter zuechter) {
		System.out.println("\tName: " + zuechter.getVorName() + " " + zuechter.getNachName());
		System.out.println("\tGeburtsdatum: " + datumZuString(zuechter.getGeburtsDatum()));
		System.out.println("\tLebensversicherungsnummer: " + zuechter.getLebensVSN());
	}

	/**
	 * Gibt den übergebenen Drachen auf der Konsole aus.
	 * 
	 * @param drache Der Drache der auszugeben ist
	 */
	public static void ausgabeDrache(Drache drache) {
		System.out.println("Name: " + drache.getName());
		System.out.println("Geburtsdatum: " + datumZuString(drache.getGeburtsDatum()));

		if (drache.getMutter() != null && drache.getVater() != null) {
			System.out.println("Eltern: " + drache.getMutter().getName() + " & " + drache.getVater().getName());
		}
	}

	/**
	 * Gibt die übergebene Farm auf der Konsole aus.
	 * Hierbei wird der zugewiesen Züchter und alle dort lebenden Drachen mit ausgegeben.
	 * 
	 * @param farm Die Farm die auszugeben ist
	 */
	public static void ausgabeFarm(DrachenFarm farm) {
		System.out.println("==================================================================");
		System.out.println("Farm: " + farm.getName());
		System.out.println("Züchter:");
		ausgabeZuechter(farm.getZuechter());
		System.out.println("==================================================================");
		for (Drache drache : farm.getDrachen()) {
			if (drache == null)
				continue;
			ausgabeDrache(drache);
			System.out.println("------------------------------------------------------------------");
		}
	}	
	
	/**
	 * Implementiere hier deine eigene Drachenfarm(en).
	 * 
	 * @param args Sind erstmal unwichtig
	 */
	public static void main(String[] args) {
		// Nun bist du an der Reihe
	}
	
}
