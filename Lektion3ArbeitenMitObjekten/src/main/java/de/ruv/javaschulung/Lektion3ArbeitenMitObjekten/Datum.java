package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

public class Datum {

	//============================
	// Felder hier implementieren
	//============================
	
	
	
	
	
	public Datum(int tag, int monat, int jahr) {
		this.setJahr(jahr);
		this.setMonat(monat);
		this.setTag(tag);
	}
	
	public Datum(int monat, int jahr) {
		this(1, monat, jahr);
	}
	
	public Datum(int jahr) {
		this(1, 1, jahr);
	}
	
	//==================================
	// Getter und Setter am besten hier
	//==================================
	
	
	
	
	public boolean istSchaltjahr() {
		boolean durch4 = this.jahr % 4 == 0;
		boolean durch100 = this.jahr % 100 == 0;
		boolean durch400 = this.jahr % 400 == 0;
		
		return durch4 && (!durch100 || durch400);
	}
	
	public boolean istKleinerAls(Datum that) {		
		if (this.jahr < that.jahr)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat < that.monat)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat == that.monat
				&& this.tag < that.tag)
			return true;
		
		return false;
	}
	
	public boolean istGroesserAls(Datum that) {
		if (this.jahr > that.jahr)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat > that.monat)
			return true;
		
		if (this.jahr == that.jahr
				&& this.monat == that.monat
				&& this.tag > that.tag)
			return true;
		
		return false;
	}
}
