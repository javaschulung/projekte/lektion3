package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

public class Drache {
	
	//============================
	// Felder hier implementieren
	//============================
	
	
	
	
	public Drache(String name, Datum geburtsDatum, Drache mutter, Drache vater) {
		this.name = name;
		this.geburtsDatum = geburtsDatum;
		this.mutter = mutter;
		this.vater = vater;
	}
	
	public Drache(String name, Datum gebDatum) {
		this(name, gebDatum, null, null);
	}
	
	//==================================
	// Getter am besten hier
	//==================================
	
	
	
	
}
