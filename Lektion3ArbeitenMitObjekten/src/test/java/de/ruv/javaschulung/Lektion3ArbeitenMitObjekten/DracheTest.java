package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DracheTest extends TestCase {

	public DracheTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(DracheTest.class);
	}

	public void testDracheKonstruktion() {
		
		Datum datum = new Datum(2000);
		
		Drache d1 = new Drache("d1", datum);
		assertEquals("d1", d1.getName());
		assertEquals(datum, d1.getGeburtsDatum());
		assertNull(d1.getMutter());
		assertNull(d1.getVater());
		
		Drache d2 = new Drache("d2", datum);
		assertEquals("d2", d2.getName());
		assertEquals(datum, d2.getGeburtsDatum());
		assertNull(d2.getMutter());
		assertNull(d2.getVater());
		
		Drache d3 = new Drache("d3", datum, d1, d2);
		assertEquals("d3", d3.getName());
		assertEquals(datum, d3.getGeburtsDatum());
		assertNotNull(d3.getMutter());
		assertEquals(d1, d3.getMutter());
		assertNotNull(d3.getVater());
		assertEquals(d2, d3.getVater());
		
		Drache d4 = new Drache("d4", datum, d2, d3);
		assertEquals("d4", d4.getName());
		assertEquals(datum, d4.getGeburtsDatum());
		assertNotNull(d4.getMutter());
		assertEquals(d2, d4.getMutter());
		assertNotNull(d4.getVater());
		assertEquals(d3, d4.getVater());
		
	}
}
