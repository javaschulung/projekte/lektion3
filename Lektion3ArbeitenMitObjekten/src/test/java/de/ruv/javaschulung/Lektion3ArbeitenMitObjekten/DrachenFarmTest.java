package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

import java.nio.BufferOverflowException;

import org.omg.PortableInterceptor.SUCCESSFUL;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DrachenFarmTest extends TestCase {
	
	public DrachenFarmTest(String testName) {
		super(testName);
	}
	
	public static Test suite() {
		return new TestSuite(DrachenFarmTest.class);
	}
	
	public void testDrachenFarmKonstruktion() {
		
		DrachenZuechter dz = new DrachenZuechter("vor", "nach", new Datum(1900), 42);
		
		DrachenFarm df1 = new DrachenFarm("df1", dz, 10);
		assertEquals("df1", df1.getName());
		assertEquals(dz, df1.getZuechter());
		assertEquals(10, df1.getDrachen().length);
		assertEquals(0, df1.getAnzahl());
		
		DrachenFarm df2 = new DrachenFarm("df2", dz, 4);
		assertEquals("df2", df2.getName());
		assertEquals(dz, df2.getZuechter());
		assertEquals(4, df2.getDrachen().length);
		assertEquals(0, df2.getAnzahl());
		
		DrachenFarm df3 = new DrachenFarm("df3", dz, 42);
		assertEquals("df3", df3.getName());
		assertEquals(dz, df3.getZuechter());
		assertEquals(42, df3.getDrachen().length);
		assertEquals(0, df3.getAnzahl());
		
	}
	
	public void testDrachenFarmAufnahme() {
		
		int max = 11;
		
		DrachenFarm df = new DrachenFarm("farm",
								new DrachenZuechter("vor", "nach", new Datum(1111), 42)
								, max);
		
		
		for (int i = 0; i < max; i++) {
			Drache d = new Drache("drache", new Datum(1));
			
			assertEquals(df.getAnzahl(), i);
			assertNull(df.getDrachen()[i]);
			
			df.aufnahme(d);
			
			assertEquals(df.getAnzahl(), i + 1);
			assertNotNull(df.getDrachen()[i]);
			assertEquals(d, df.getDrachen()[i]);
		}
		
	}
	
	public void testDrachenFarmAufnahmeOverflow() {
		
		int max = 3;
		
		DrachenFarm df = new DrachenFarm("farm",
								new DrachenZuechter("vor", "nach", new Datum(1111), 42)
								, max);
		
		
		for (int i = 0; i < max; i++) {
			Drache d = new Drache("drache", new Datum(1));
			
			assertEquals(df.getAnzahl(), i);
			assertNull(df.getDrachen()[i]);
			
			df.aufnahme(d);
			
			assertEquals(df.getAnzahl(), i + 1);
			assertNotNull(df.getDrachen()[i]);
			assertEquals(d, df.getDrachen()[i]);
		}
		
		Drache d = new Drache("dragon", new Datum(2));
		
		assertEquals(max, df.getAnzahl());
		
		assertNotSame(d, df.getDrachen()[max-1]);
		
		
		try {
			df.aufnahme(d);

		    fail( "Missing exception" );
		} catch( BufferOverflowException e ) {
		     //assertEquals( "Expected message", e.getMessage() ); // Optionally make sure you get the correct message, too
			assertTrue(true);
		}
			
		
		assertEquals(max, df.getAnzahl());
		
		assertNotSame(d, df.getDrachen()[max-1]);
		
	}
	
}
