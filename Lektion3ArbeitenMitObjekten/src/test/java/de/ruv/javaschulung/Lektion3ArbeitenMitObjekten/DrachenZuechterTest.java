package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DrachenZuechterTest extends TestCase{
	
	public DrachenZuechterTest(String testName) {
		super(testName);
	}
	
	public static Test suite() {
		return new TestSuite(DrachenZuechterTest.class);
	}
	
	public void testDrachenZuechterKonstruktion() {
		
		Datum datum = new Datum(1999);
		
		DrachenZuechter z1 = new DrachenZuechter("vor1", "nach1", datum, 42);
		assertEquals("vor1", z1.getVorName());
		assertEquals("nach1", z1.getNachName());
		assertEquals(datum, z1.getGeburtsDatum());
		assertEquals(42, z1.getLebensVSN());
		
		DrachenZuechter z2 = new DrachenZuechter("vor2", "nach2", datum, 7);
		assertEquals("vor2", z2.getVorName());
		assertEquals("nach2", z2.getNachName());
		assertEquals(datum, z2.getGeburtsDatum());
		assertEquals(7, z2.getLebensVSN());
	}

}
