package de.ruv.javaschulung.Lektion3ArbeitenMitObjekten;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DatumTest extends TestCase {

	public DatumTest(String testName) {
		super(testName);
	}
	
	public static Test suite() {
		return new TestSuite(DatumTest.class);
	}
	
	public void testDatumKonstruktion() {
		
		Datum d1 = new Datum(13, 6, 1999);
		assertEquals(13, d1.getTag());
		assertEquals(6, d1.getMonat());
		assertEquals(1999, d1.getJahr());

		Datum d2 = new Datum(7, 1856);
		assertEquals(1, d2.getTag());
		assertEquals(7, d2.getMonat());
		assertEquals(1856, d2.getJahr());
		
		Datum d3 = new Datum(2001);
		assertEquals(1, d3.getTag());
		assertEquals(1, d3.getMonat());
		assertEquals(2001, d3.getJahr());
		
		Datum d4 = new Datum(24, 12, 2007);
		assertEquals(24, d4.getTag());
		assertEquals(12, d4.getMonat());
		assertEquals(2007, d4.getJahr());
		
	}
	
	public void testDatumSchaltjahr() {
		
		Datum d1 = new Datum(2001);
		assertFalse("Ist ein Schaltjahr", d1.istSchaltjahr());
		
		Datum d2 = new Datum(1956);
		assertFalse("Ist ein Schaltjahr", d1.istSchaltjahr());
		
		Datum d3 = new Datum(1448);
		assertTrue("Ist kein Schaltjahr", d3.istSchaltjahr());
		
		Datum d4 = new Datum(2008);
		assertTrue("Ist kein Schaltjahr", d4.istSchaltjahr());
		
	}
	
	public void testDatumKleinerAls() {
		
		Datum d1 = new Datum(13, 6, 1999);
		Datum d2 = new Datum(13, 6, 2000);
		
		Datum d3 = new Datum(24, 6, 1856);
		Datum d4 = new Datum(24, 7, 1856);
		
		Datum d5 = new Datum(10, 3, 2001);
		Datum d6 = new Datum(11, 3, 2001);
		
		Datum d7 = new Datum(24, 12, 2007);
		
		assertTrue("A ist nicht kleiner als B", d1.istKleinerAls(d2));
		assertFalse("B ist kleiner als A", d2.istKleinerAls(d1));
		
		assertTrue("A ist nicht kleiner als B", d3.istKleinerAls(d4));
		assertFalse("B ist kleiner als A", d4.istKleinerAls(d3));
		
		assertTrue("A ist nicht kleiner als B", d5.istKleinerAls(d6));
		assertFalse("B ist kleiner als A", d6.istKleinerAls(d5));
		
		assertFalse("A ist kleiner als A", d7.istKleinerAls(d7));
		assertFalse("A ist kleiner als A", d7.istKleinerAls(d7));
	}
	
	public void testDatumGroesserAls() {
		
		Datum d1 = new Datum(13, 6, 2000);
		Datum d2 = new Datum(13, 6, 1999);
		
		Datum d3 = new Datum(24, 7, 1856);
		Datum d4 = new Datum(24, 6, 1856);
		
		Datum d5 = new Datum(11, 3, 2001);
		Datum d6 = new Datum(10, 3, 2001);
		
		Datum d7 = new Datum(24, 12, 2007);
		
		assertTrue("A ist nicht größer als B", d1.istGroesserAls(d2));
		assertFalse("B ist größer als A", d2.istGroesserAls(d1));
		
		assertTrue("A ist nicht größer als B", d3.istGroesserAls(d4));
		assertFalse("B ist größer als A", d4.istGroesserAls(d3));
		
		assertTrue("A ist nicht größer als B", d5.istGroesserAls(d6));
		assertFalse("B ist größer als A", d6.istGroesserAls(d5));
		
		assertFalse("A ist größer als A", d7.istGroesserAls(d7));
		assertFalse("A ist größer als A", d7.istGroesserAls(d7));
	}
	
public void testDatumGleicherTagAls() {
		
		Datum d1 = new Datum(13, 6, 2000);
		Datum d2 = new Datum(13, 6, 1999);
		
		Datum d3 = new Datum(24, 7, 1856);
		Datum d4 = new Datum(24, 6, 1856);
		
		Datum d5 = new Datum(11, 3, 2001);
		Datum d6 = new Datum(10, 3, 2001);
		
		Datum d7 = new Datum(24, 12, 2007);
		Datum d8 = new Datum(24, 12, 2007);
		
		assertFalse("A ist gleich B", d1.istGleicherTagAls(d2));
		assertFalse("B ist gleich A", d2.istGleicherTagAls(d1));
		
		assertFalse("A ist gleich B", d3.istGleicherTagAls(d4));
		assertFalse("B ist gleich A", d4.istGleicherTagAls(d3));
		
		assertFalse("A ist gleich B", d5.istGleicherTagAls(d6));
		assertFalse("B ist gleich A", d6.istGleicherTagAls(d5));
		
		assertTrue("A ist nicht gleich A", d7.istGleicherTagAls(d7));
		assertTrue("A ist nicht gleich A", d7.istGleicherTagAls(d8));
		assertTrue("A ist nicht gleich A", d8.istGleicherTagAls(d7));
	}
}
